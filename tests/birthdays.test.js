import { expect, test } from '@playwright/test';

import {
  BirthdayListPage
} from './BirthdayListPage.js';

test('lists all birthdays', async ({ page }) => {
  const birthdayListPage = new BirthdayListPage(page);
  await birthdayListPage.goto();

  await expect(
    page.getByText('Hercules')
  ).toBeVisible();
  await expect(
    page.getByText('Athena')
  ).toBeVisible();
});

test('saves a new birthday', async ({ page }) => {
  const birthdayListPage = new BirthdayListPage(page);
  await birthdayListPage.goto();

  await page.getByLabel('Name').fill('Persephone');
  await page
    .getByLabel('Date of birth')
    .fill('1985-01-01');

  
  await page.getByRole('button',
    { name: 'Save' }
  ).click();
  await expect(
    page.getByText('Persephone')
  ).toBeVisible();
});

test('does not save a birthday if there are validation errors', async ({
    page
}) => {
  const birthdayListPage = new BirthdayListPage(page);
  await birthdayListPage.goto();

  await page.getByLabel('Name').fill('Demeter');
  await page
    .getByLabel('Date of birth')
    .fill('invalid');
  await page.getByRole('button',
    { name: 'Save' }
  ).click();

  await expect(
    page.getByText('Demeter')
  ).not.toBeVisible();
  await expect(
    page.getByText(
      'Please provide a date of birth in the YYYY-MM-DD format.'
    )
  ).toBeVisible();
});

test('edits a birthday', async ({ page }) => {
  const birthdayListPage = new BirthdayListPage(page);
  await birthdayListPage.goto();

  // add a birthday using the form
  await birthdayListPage.saveNameAndDateOfBirth(
    'Ares',
    '1985-01-01'
  );
  // await page.getByLabel('Name').fill('Ares');
  // await page
  //   .getByLabel('Date of birth')
  //   .fill('1985-01-01');
  // await page
  //   .getByRole('button', { name: 'Save' })
  //   .click();

  // find edit button on the added birthday
  await birthdayListPage.beginEditingFor('Ares');
  // await page
  //   .getByRole('listitem')
  //   .filter({ hasText: 'Ares' })
  //   .getByRole('button', { name: 'Edit' })
  //   .click();
  
  // Replace Date of Birth with a new value
  await birthdayListPage.saveNameAndDateOfBirth(
    'Ares',
    '1995-01-01'
  );
  // await page
  //   .getByLabel('Date of birth')
  //   .fill('1995-01-01');
  // await page
  //   .getByRole('button', { name: 'Save' })
  //   .click();

  // check that the original text doesn't appear
  await expect(
    birthdayListPage.entryFor('Ares')
    // page
    //   .getByRole('listitem')
    //   .filter({ hasText: 'Ares' })
  ).not.toContainText('1985-01-01');
  // check that the new text does appear
  await expect(
    birthdayListPage.entryFor('Ares')
    // page
    //   .getByRole('listitem')
    //   .filter({ hasText: 'Ares' })
  ).toContainText('1995-01-01');
});